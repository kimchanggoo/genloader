﻿using GenericClass;
using GenericClass.Define;
using GenericClass.Enum;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using FileBrowser;

namespace Genload
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            GenBrowser.filter = "*.Gen";
            NcBrowser.filter = "*.mpg";
        }

        private void btn_Converter_Click(object sender, EventArgs e)
        {
            //CGenInfo genInfo = new CGenInfo();

            //genInfo.LoadSpeedTable(DefineValue.SpeedTableFilePath);

            //genInfo.Load(tBox_Path.Text);

            //string ncNameTEP = tBox_Path.Text.ToString().ToUpper().Replace(".GEN", "_TEP.mpg");
            //string ncNameTBC = tBox_Path.Text.ToString().ToUpper().Replace(".GEN", "_TBC.mpg");
            //string ncNameTBP = tBox_Path.Text.ToString().ToUpper().Replace(".GEN", "_TBP.mpg");

            //genInfo.SetGeoList();

            //genInfo.ConvertEssiCode(ncNameTEP, Machine.TEP);
        }

        public bool ConvertGeneric(CGenInfo cGenInfo)
        {
            bool bRtn = true;

            if (!cGenInfo.LoadSpeedTable())
            {
                return false;
            }

            if (!cGenInfo.LoadGenericFile())
            {
                return false;
            }

            cGenInfo.SetGeoList();

            if (!cGenInfo.ConvertEssiCode())
            {
                return false;
            }

            return bRtn;
        }
        // Essi 코드를 write 하는 로직은 TBP, TBC, TEP 타입과 관계없이 일정하게 쓴다.
        // 
        // TBC
        private void button1_Click(object sender, EventArgs e)
        {
            List<FileInfo> result = GenBrowser.getSelectedFileInfo();
            string messageString = "";
            
            // 변수 선언
            string speedFilePath = "SF842DBEV_ESSI.DAT";
            bool bWriteLetterMark = false;
            bool bWriteLineMarking = true;
            bool bWriteSpeed = false;
            bool bWriteBevel = true;
            CuttingOption cuttingOption = CuttingOption.C;
            string genericFilePath = "";
            string essiFilePath = "";
            Machine machinePackage = Machine.TBC;

            foreach(var fInfo in result)
            {
                genericFilePath = fInfo.FullName;
                essiFilePath = genericFilePath.Substring(0, genericFilePath.IndexOf(fInfo.Extension)) + ".mpg";

                CGenInfo cGenInfo = new CGenInfo();
                cGenInfo.bWriteText = bWriteLetterMark;
                cGenInfo.bWriteMarking = bWriteLineMarking;
                cGenInfo.bWriteSpeed = bWriteSpeed;
                cGenInfo.bWriteBevel = bWriteBevel;
                cGenInfo.SpeedFilePath = speedFilePath;
                cGenInfo.GenericFilePath = genericFilePath;
                cGenInfo.EssiFilePath = essiFilePath;
                cGenInfo.MachinePackage = machinePackage;
                cGenInfo.CuttingOptionPackage = cuttingOption;

                bool bOk = ConvertGeneric(cGenInfo);

                messageString += string.Format("[{0}] ", cGenInfo.generalInfo.nest_name);
                if (cGenInfo.bError)
                {
                    messageString += "Error\n";
                    if (cGenInfo.bError)
                    {
                        foreach (var item in cGenInfo.err_info)
                        {
                            messageString += item.ToString() + "\n";
                        }
                    }
                }
                else
                {
                    messageString += "Success\n";
                }
            }
            MessageBox.Show(messageString);

        }

        // TBP
        private void button2_Click(object sender, EventArgs e)
        {
            List<FileInfo> result = GenBrowser.getSelectedFileInfo();
            string messageString = "";

            // 변수 선언
            string speedFilePath = "SF842DBEV_ESSI.DAT";
            bool bWriteLetterMark = false;
            bool bWriteLineMarking = true;
            bool bWriteSpeed = false;
            bool bWriteBevel = true;
            CuttingOption cuttingOption = CuttingOption.C;
            string genericFilePath = "";
            string essiFilePath = "";
            Machine machinePackage = Machine.TBP;

            foreach (var fInfo in result)
            {
                genericFilePath = fInfo.FullName;
                essiFilePath = genericFilePath.Substring(0, genericFilePath.IndexOf(fInfo.Extension)) + ".mpg";

                CGenInfo cGenInfo = new CGenInfo();
                cGenInfo.bWriteText = bWriteLetterMark;
                cGenInfo.bWriteMarking = bWriteLineMarking;
                cGenInfo.bWriteSpeed = bWriteSpeed;
                cGenInfo.bWriteBevel = bWriteBevel;
                cGenInfo.SpeedFilePath = speedFilePath;
                cGenInfo.GenericFilePath = genericFilePath;
                cGenInfo.EssiFilePath = essiFilePath;
                cGenInfo.MachinePackage = machinePackage;
                cGenInfo.CuttingOptionPackage = cuttingOption;

                bool bOk = ConvertGeneric(cGenInfo);

                messageString += string.Format("[{0}] ", cGenInfo.generalInfo.nest_name);
                if (cGenInfo.bError)
                {
                    messageString += "Error\n";
                    if (cGenInfo.bError)
                    {
                        foreach (var item in cGenInfo.err_info)
                        {
                            messageString += item.ToString() + "\n";
                        }
                    }
                }
                else
                {
                    messageString += "Success\n";
                }
            }
            MessageBox.Show(messageString);
        }
        
        // TEP
        // bevel i cutting 처리 할 경우
        // load table, Load, SetGeoList, 순서 볼 것.
        private void button3_Click(object sender, EventArgs e)
        {
            List<FileInfo> result = GenBrowser.getSelectedFileInfo();
            string messageString = "";

            // 변수 선언
            string speedFilePath = "SF842DBEV_ESSI.DAT";
            bool bWriteLetterMark = true;
            bool bWriteLineMarking = true;
            bool bWriteSpeed = true;
            bool bWriteBevel = true;
            CuttingOption cuttingOption = CuttingOption.C;
            string genericFilePath = "";
            string essiFilePath = "";
            Machine machinePackage = Machine.TEP;

            foreach (var fInfo in result)
            {
                genericFilePath = fInfo.FullName;
                essiFilePath = genericFilePath.Substring(0, genericFilePath.IndexOf(fInfo.Extension)) + ".mpg";

                CGenInfo cGenInfo = new CGenInfo();
                cGenInfo.bWriteText = bWriteLetterMark;
                cGenInfo.bWriteMarking = bWriteLineMarking;
                cGenInfo.bWriteSpeed = bWriteSpeed;
                cGenInfo.bWriteBevel = bWriteBevel;
                cGenInfo.SpeedFilePath = speedFilePath;
                cGenInfo.GenericFilePath = genericFilePath;
                cGenInfo.EssiFilePath = essiFilePath;
                cGenInfo.MachinePackage = machinePackage;
                cGenInfo.CuttingOptionPackage = cuttingOption;

                bool bOk = ConvertGeneric(cGenInfo);

                messageString += string.Format("[{0}] ", cGenInfo.generalInfo.nest_name);
                if (cGenInfo.bError)
                {
                    messageString += "Error\n";
                    if (cGenInfo.bError)
                    {
                        foreach (var item in cGenInfo.err_info)
                        {
                            messageString += item.ToString() + "\n";
                        }
                    }
                }
                else
                {
                    messageString += "Success\n";
                }
            }
            MessageBox.Show(messageString);
        }
    }
}
