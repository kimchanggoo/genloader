﻿namespace Genload
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.tBox_Path = new System.Windows.Forms.TextBox();
            this.btn_Path = new System.Windows.Forms.Button();
            this.btn_Converter = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.NcBrowser = new FileBrowser.Browser();
            this.GenBrowser = new FileBrowser.Browser();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tBox_Path
            // 
            this.tBox_Path.Location = new System.Drawing.Point(12, 12);
            this.tBox_Path.Name = "tBox_Path";
            this.tBox_Path.Size = new System.Drawing.Size(575, 21);
            this.tBox_Path.TabIndex = 0;
            this.tBox_Path.Text = "D:\\NAVANTIA\\temp\\test.gen";
            // 
            // btn_Path
            // 
            this.btn_Path.Location = new System.Drawing.Point(593, 12);
            this.btn_Path.Name = "btn_Path";
            this.btn_Path.Size = new System.Drawing.Size(75, 23);
            this.btn_Path.TabIndex = 1;
            this.btn_Path.Text = "파일선택";
            this.btn_Path.UseVisualStyleBackColor = true;
            // 
            // btn_Converter
            // 
            this.btn_Converter.Location = new System.Drawing.Point(674, 12);
            this.btn_Converter.Name = "btn_Converter";
            this.btn_Converter.Size = new System.Drawing.Size(75, 23);
            this.btn_Converter.TabIndex = 2;
            this.btn_Converter.Text = "컨버팅";
            this.btn_Converter.UseVisualStyleBackColor = true;
            this.btn_Converter.Click += new System.EventHandler(this.btn_Converter_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GenBrowser);
            this.groupBox1.Location = new System.Drawing.Point(12, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(329, 528);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gen Path";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.NcBrowser);
            this.groupBox2.Location = new System.Drawing.Point(420, 41);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(329, 528);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "NC Path";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(343, 242);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = ">>TBC";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(343, 271);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = ">>TBP";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(343, 300);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = ">>TEP";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // NcBrowser
            // 
            this.NcBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NcBrowser.Location = new System.Drawing.Point(3, 17);
            this.NcBrowser.Name = "NcBrowser";
            this.NcBrowser.Size = new System.Drawing.Size(323, 508);
            this.NcBrowser.TabIndex = 0;
            // 
            // GenBrowser
            // 
            this.GenBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GenBrowser.Location = new System.Drawing.Point(3, 17);
            this.GenBrowser.Name = "GenBrowser";
            this.GenBrowser.Size = new System.Drawing.Size(323, 508);
            this.GenBrowser.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 579);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Converter);
            this.Controls.Add(this.btn_Path);
            this.Controls.Add(this.tBox_Path);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBox_Path;
        private System.Windows.Forms.Button btn_Path;
        private System.Windows.Forms.Button btn_Converter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private FileBrowser.Browser GenBrowser;
        private FileBrowser.Browser NcBrowser;
    }
}

