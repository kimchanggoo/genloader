﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace FileBrowser
{
    public partial class Browser : UserControl
    {
        private string _path;
        public string filter = "*.*";


        public Browser()
        {
            InitializeComponent();
        }

        private void btn_Folder_Click(object sender, EventArgs e)
        {
            if (folderDlg.ShowDialog() == DialogResult.OK)
            {
                _path = folderDlg.SelectedPath;
                tBox_Path.Text = _path;
                SetFolder();
            }
        }

        private void SetFolder()
        {
            lBox_File.Items.Clear();

            if (!Directory.Exists(_path))
            {
                MessageBox.Show("올바른 Path가 아닙니다.");
                return;
            }
            
            string[] filePaths = Directory.GetFiles(_path, filter);

            foreach (var fileinfo in filePaths)
            {
                FileInfo fInfo = new FileInfo(fileinfo);
                lBox_File.Items.Add(fInfo.Name);
            }
            

        }

        private void tBox_Path_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                _path = tBox_Path.Text;
                SetFolder();
            }

        }

        public List<FileInfo> getSelectedFileInfo()
        {
            List<FileInfo> result = new List<FileInfo>();

            foreach (var obj in lBox_File.SelectedItems)
            {
                string tPath = Path.Combine(_path, obj.ToString());
                if (!File.Exists(tPath))
                {
                    continue;
                }

                FileInfo fInfo = new FileInfo(tPath);
                result.Add(fInfo);
            }

            return result;

        }
    }
}
