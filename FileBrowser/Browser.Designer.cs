﻿namespace FileBrowser
{
    partial class Browser
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tBox_Path = new System.Windows.Forms.TextBox();
            this.btn_Folder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.folderDlg = new System.Windows.Forms.FolderBrowserDialog();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lBox_File = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // tBox_Path
            // 
            this.tBox_Path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tBox_Path.Location = new System.Drawing.Point(61, 3);
            this.tBox_Path.Name = "tBox_Path";
            this.tBox_Path.Size = new System.Drawing.Size(298, 21);
            this.tBox_Path.TabIndex = 0;
            this.tBox_Path.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tBox_Path_KeyDown);
            // 
            // btn_Folder
            // 
            this.btn_Folder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Folder.Location = new System.Drawing.Point(365, 1);
            this.btn_Folder.Name = "btn_Folder";
            this.btn_Folder.Size = new System.Drawing.Size(35, 23);
            this.btn_Folder.TabIndex = 1;
            this.btn_Folder.Text = "...";
            this.btn_Folder.UseVisualStyleBackColor = true;
            this.btn_Folder.Click += new System.EventHandler(this.btn_Folder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "경로 : ";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // lBox_File
            // 
            this.lBox_File.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lBox_File.FormattingEnabled = true;
            this.lBox_File.ItemHeight = 12;
            this.lBox_File.Location = new System.Drawing.Point(3, 30);
            this.lBox_File.Name = "lBox_File";
            this.lBox_File.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lBox_File.Size = new System.Drawing.Size(397, 328);
            this.lBox_File.TabIndex = 3;
            // 
            // Browser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lBox_File);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Folder);
            this.Controls.Add(this.tBox_Path);
            this.Name = "Browser";
            this.Size = new System.Drawing.Size(404, 364);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBox_Path;
        private System.Windows.Forms.Button btn_Folder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog folderDlg;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ListBox lBox_File;

    }
}
