﻿using GenericClass.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass.GenType
{
    public class CLine
    {
        public NCType Type { get; set; }
        public NCSubType SubType { get; set; }
        public LineType LineType { get; set; }
        public CPoint StartPoint { get; set; }
        public CPoint EndPoint { get; set; }
        public CPoint CenterPoint { get; set; }
        public double Radius { get; set; }
        public int dir { get; set; }
        public double Bevel { get; set; }

        public CLine()
        {
            Type = NCType.IDLE;
            SubType = NCSubType.NONE;
            StartPoint = new CPoint();
            EndPoint = new CPoint();
            CenterPoint = new CPoint();
            Bevel = 0;
            Radius = 0;
        }

        public CLine Clone()
        {
            if (this == null) return null;

            CLine newLine = new CLine();
            newLine.Type = this.Type;
            newLine.SubType = this.SubType;
            newLine.StartPoint.X = this.StartPoint.X;
            newLine.StartPoint.Y = this.StartPoint.Y;
            newLine.EndPoint.X = this.EndPoint.X;
            newLine.EndPoint.Y = this.EndPoint.Y;
            newLine.CenterPoint.X = this.CenterPoint.X;
            newLine.CenterPoint.Y = this.CenterPoint.Y;
            newLine.Bevel = this.Bevel;
            newLine.Radius = this.Radius;
            newLine.LineType = this.LineType;
            newLine.dir = this.dir;

            return newLine;
        }
    }

}
