﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass.GenType
{
    public class CPoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public CPoint()
        {
            X = 0;
            Y = 0;
        }

        public CPoint(double xx, double yy)
        {
            X = xx;
            Y = yy;
        }
    }
}
