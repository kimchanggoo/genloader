﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass.UTIL
{
    public static class CommonUtil
    {
        public static string ListToJsonString(List<Tuple<string,string>> input)
        {
            string sRtn = "{";

            foreach (Tuple<string,string> obj in input)
            {
                sRtn += string.Format("\"{0}\":\"{1}\",", obj.Item1, obj.Item2.Replace("\"", "\\\""));
            }
            sRtn += "}";
            return sRtn;
        }


        
    }
}
