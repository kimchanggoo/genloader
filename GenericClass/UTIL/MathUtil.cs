﻿using GenericClass.GenType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass.UTIL
{
    public static class MathUtil
    {
        public const double M_PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862;

        public static double RadianToDegree(double rAng)
        {
            double dRtn = (rAng / M_PI) * 180.0f;
            return dRtn;
        }

        public static double DegreeToRadian(double dAng)
        {
            double dRtn = (dAng / 180.0f) * M_PI;
            return dRtn;
        }

        public static double GetAngle(CPoint p1, CPoint p2)
        {
            Double xdf = p2.X - p1.X;
            Double ydf = p2.Y - p1.Y;

            return Math.Atan2(ydf, xdf);// +DegreeToRadian(90);
        }

        public static Double GetDistance(CPoint p1, CPoint p2)
        {
            Double xdf = p2.X - p1.X;
            Double ydf = p2.Y - p1.Y;
            return Math.Sqrt(Math.Pow(xdf, 2) + Math.Pow(ydf, 2));
        }

        public static CPoint GetDirection(Double deg)
        {
            return new CPoint(-(Single)Math.Sin(DegreeToRadian(deg)),
                              (Single)Math.Cos(DegreeToRadian(deg)));
        }

        public static CPoint cal_LineDistancePt(CPoint startPoint, CPoint endPoint, double Dist)
        {
            //두선의 각도를 구함
            double rad_ang = GetAngle(startPoint, endPoint);

            //두선의 각도가 360도 이상이면 간소화 시킴
            rad_ang = MathUtil.dAngle(rad_ang);


            CPoint returnValue = new CPoint();
            returnValue.X = (float)((double)startPoint.X + Dist * Math.Cos(rad_ang));
            returnValue.Y = (float)((double)startPoint.Y + Dist * Math.Sin(rad_ang));
            return returnValue;
        }
        
        public static CPoint cal_ArcDistancePt(CPoint startPoint, CPoint endPoint, CPoint cPoint, double cr, double len)
        {
            double clen, rate, rad, srad, erad;

            srad = GetAngle(cPoint, startPoint);
            erad = GetAngle(cPoint, endPoint);

            clen = Math.Abs(cr) * M_PI * 2.0;
            rate = len / clen;
            rad = M_PI * 2.0 * rate;
            if (cr > 0) srad += rad;
            else srad -= rad;

            CPoint returnValue = new CPoint();
            returnValue.X = cPoint.X + Math.Cos(srad) * Math.Abs(cr); ;
            returnValue.Y = cPoint.Y + Math.Sin(srad) * Math.Abs(cr);;

            return returnValue;
        }
        
        public static Double cal_LineLen(CPoint p1, CPoint p2)
        {
            Double xdf = p2.X - p1.X;
            Double ydf = p2.Y - p1.Y;
            return Math.Sqrt(Math.Pow(xdf, 2) + Math.Pow(ydf, 2));
        }

        public static double cal_ArcLen(CPoint cp, double cr, CPoint sp, CPoint ep)
        {
            double eps, ank, srad, erad;

            srad = GetAngle(cp,sp);
            erad = GetAngle(cp,ep);

            eps = 0.0000001;
            if (srad > erad) erad = erad + 2.0 * M_PI;
            ank = Math.Abs(erad - srad);
            if (Math.Abs(ank) < eps) return (2.0 * M_PI * cr); // full circle
            if (cr < 0) ank = 2.0 * M_PI - ank;
            return (Math.Abs(cr * ank));
        }

        //360도 이상이면 360도를 빼고 나머지 각도로 단순화 시킴
        private static double dAngle(double Angle)
        {
            double result;
            if (Angle > 6.2831853071795862)
            {
                result = Angle - 6.2831853071795862;
            }
            else if (Angle < 0.0)
            {
                result = Angle + 6.2831853071795862;
            }
            else
            {
                result = Angle;
            }
            return result;
        }

        
    }
}
