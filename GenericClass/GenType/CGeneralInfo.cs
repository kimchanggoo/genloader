﻿using GenericClass.Define;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass.GenType
{
    public class CGeneralInfo
    {
        [JsonProperty(DefineProperty.NEST_NAME)]
        public string nest_name { get; set; }

        [JsonProperty(DefineProperty.RAW_LENGTH)]
        public double raw_length { get; set; }

        [JsonProperty(DefineProperty.RAW_WIDTH)]
        public double raw_width { get; set; }

        [JsonProperty(DefineProperty.RAW_THICKNESS)]
        public double raw_thickness { get; set; }

        [JsonProperty(DefineProperty.QUALITY)]
        public string quality { get; set; }

        
    }
}
