﻿using GenericClass.Define;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass
{
    public class CBevelData
    {
        [JsonProperty(DefineProperty.BEVEL)]
        public string bevel { get; set; }

        [JsonProperty(DefineProperty.BEVEL_CODE)]
        public string bevel_code { get; set; }

        [JsonProperty(DefineProperty.BEVEL_TYPE)]
        public string bevel_type { get; set; }

        [JsonProperty(DefineProperty.PLATE_THICKNESS)]
        public double plate_thick { get; set;}

        [JsonProperty(DefineProperty.ANGLE_TS)]
        public double angle_ts { get; set; }

        [JsonProperty(DefineProperty.ANGLE2_TS)]
        public double angle2_ts { get; set; }

        [JsonProperty(DefineProperty.ANGLE_OS)]
        public double angle_os { get; set; }

        [JsonProperty(DefineProperty.ANGLE2_OS)]
        public double angle2_os { get; set; }

        [JsonProperty(DefineProperty.DEPTH_TS)]
        public double depth_ts { get; set; }

        [JsonProperty(DefineProperty.DEPTH_OS)]
        public double depth_os { get; set; }

        [JsonProperty(DefineProperty.CHAMFER_WIDTH_TS)]
        public double chamfer_width_ts { get; set; }

        [JsonProperty(DefineProperty.CHAMFER_WIDTH_OS)]
        public double chamfer_width_os { get; set; }

        [JsonProperty(DefineProperty.ANGLE2_WTS)]
        public double angle2_wts { get; set; }

        [JsonProperty(DefineProperty.ANGLE2_WOS)]
        public double angle2_wos { get; set; }

        [JsonProperty(DefineProperty.CHAMFER_HEIGHT_TS)]
        public double chamfer_height_ts { get; set; }

        [JsonProperty(DefineProperty.CHAMFER_HEIGHT_OS)]
        public double chamfer_height_os { get; set; }
    }
}
