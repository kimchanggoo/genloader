﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using GenericClass.Define;

namespace GenericClass
{
    public class CTextData
    {
        [JsonProperty(DefineProperty.TEXT_TYPE)]
        public string text_type { get; set; }

        [JsonProperty(DefineProperty.TEXT_POSITION_U)]
        public double position_u { get; set; }

        [JsonProperty(DefineProperty.TEXT_POSITION_V)]
        public double position_v { get; set; }

        [JsonProperty(DefineProperty.TEXT_ANGLE)]
        public double angle { get; set; }

        [JsonProperty(DefineProperty.TEXT_HEIGHT)]
        public double height { get; set; }

        [JsonProperty(DefineProperty.TEXT)]
        public string text { get; set; }

        [JsonProperty(DefineProperty.TEXT_ELEMENTS)]
        public string elements { get; set; }
    }
}
