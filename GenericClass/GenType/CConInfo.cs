﻿using GenericClass.Define;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass
{
    public class CConInfo
    {
        [JsonProperty(DefineProperty.NO_OF_SEG)]
        public int no_of_seq { get; set; }
        
        [JsonProperty(DefineProperty.START_U)]
        public double start_u { get; set; }

        [JsonProperty(DefineProperty.START_V)]
        public double start_v { get; set; }

        [JsonIgnore]
        public CBevelData bevel_data { get; set; }

        [JsonIgnore]
        public List<CSegInfo> seqList { get; set; }
        
    }
}
