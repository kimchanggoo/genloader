﻿using GenericClass.Define;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass
{
    public class CPartInfo
    {
        [JsonProperty(DefineProperty.PART_NAME)]
        public string part_name { get; set; }

        [JsonProperty(DefineProperty.TYPE_OF_WORK)]
        public string work_type { get; set; }        
    }
}
