﻿using GenericClass.Define;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass
{
    public class CData_Burning : CSegType
    {
        [JsonProperty(DefineProperty.SHAPE)]
        public string shape { get; set; }

        [JsonProperty(DefineProperty.START_END_IN_GAP)]
        public string start_end_in_gap { get; set; }

        [JsonProperty(DefineProperty.BEVEL_DEFINED)]
        public string bevel_defined { get; set; }

        [JsonProperty(DefineProperty.DIRECTION)]
        public int direction { get; set; }

        [JsonProperty(DefineProperty.NUMBER_OF_HEADS)]
        public string number_of_heads { get; set; }

        [JsonProperty(DefineProperty.GEOMETRY_VALID_FOR)]
        public string geometry_valid_for { get; set; }

        [JsonProperty(DefineProperty.DISTANCE_Y1_Y2)]
        public double distance_v1_v2 { get; set; }

    }
}
