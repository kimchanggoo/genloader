﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GenericClass.Define;
using GenericClass.UTIL;
using Newtonsoft.Json;
using GenericClass.GenType;
using GenericClass.Enum;

namespace GenericClass
{
    public class CGenInfo
    {
        public List<CTextData> textInfo { get; set; }
        public List<Tuple<CPartInfo, CSegType, CConInfo>> partInfo { get; set; }

        public CGeneralInfo generalInfo { get; set; }

        public List<CLine> geo { get; set; }

        public List<string> mainCode { get; set; }

        public List<string> err_info { get; set; }
        public List<string> log_info { get; set; }

        public Dictionary<Tuple<double, int>, Tuple<int, int>> speedTable { get; set; }

        // Label Text 
        public bool bWriteText { get; set; }
        // Line Marking
        public bool bWriteMarking { get; set; }
        // Speed Code
        public bool bWriteSpeed { get; set; }
        // Bevel Cut or Vertical Cut
        public bool bWriteBevel { get; set; }

        // Speed File Name
        public string SpeedFilePath { get; set; }
        // Generic File Name
        public string GenericFilePath { get; set; }
        // Essi Code File Name
        public string EssiFilePath { get; set; }
        // Machine Package ( Work Package )  TBC, TBP, TEP
        public Machine MachinePackage { get; set; }

        public CuttingOption CuttingOptionPackage { get; set; }

        // tangentila control off
        public bool bTangentialControl { get; set; }
        // marking offset off
        public bool bMarkingOffsetOff { get; set; }
        
        public bool bError
        {
            get
            {
                if (err_info.Count() >= 1) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        public CGenInfo()
        {
            textInfo = new List<CTextData>();
            partInfo = new List<Tuple<CPartInfo, CSegType, CConInfo>>();
            geo = new List<CLine>();
            
            mainCode = new List<string>();
            err_info = new List<string>();
            log_info = new List<string>();

            speedTable = new Dictionary<Tuple<double, int>, Tuple<int, int>>();
            
            bWriteText = true;
            bWriteMarking = true;
            bWriteSpeed = true;
            bWriteBevel = true;

            SpeedFilePath = "";
            GenericFilePath = "";
            EssiFilePath = "";
            MachinePackage = Machine.None;
            CuttingOptionPackage = CuttingOption.None;

            bTangentialControl = false;
            bMarkingOffsetOff = true;

        }

        private List<Tuple<string, List<Tuple<string, string>>>> ListToDetail(List<Tuple<string, string>> input)
        {
            List<Tuple<string, List<Tuple<string, string>>>> result = new List<Tuple<string, List<Tuple<string, string>>>>();
            List<Tuple<string, string>> tList = new List<Tuple<string, string>>();
            int nIdx = 1;
            string Prefix = "SEG";

            foreach (Tuple<string, string> obj in input)
            {
                if (obj.Item1.Equals("SUBTYPE") && obj.Item2.Equals("START_OF_CONTOUR"))
                {
                    if (tList.Count() >= 1)
                    {
                        result.Add(new Tuple<string, List<Tuple<string, string>>>("INFORM", tList));
                        tList = new List<Tuple<string, string>>();
                    }
                    continue;
                }
                else if (obj.Item1.Equals("SUBTYPE") && obj.Item2.Equals("END_OF_CONTOUR"))
                {
                    break;                    
                }
                else if (obj.Item1.Equals("SUBTYPE") && obj.Item2.Equals("BEVEL_DATA"))
                {                   
                    tList = new List<Tuple<string, string>>();
                    continue;
                }
                else if (obj.Item1.Equals("SUBTYPE") && obj.Item2.Equals("END_OF_BEVEL_DATA"))
                {
                    if (tList.Count() >= 1)
                    {
                        result.Add(new Tuple<string, List<Tuple<string, string>>>("BEVEL_DATA", tList));
                        tList = new List<Tuple<string, string>>();
                    }
                    continue;
                }
                else if (obj.Item1.Equals("START_V"))
                {
                    if (tList.Count() >= 1)
                    {
                        tList.Add(obj);
                        result.Add(new Tuple<string, List<Tuple<string, string>>>("CON_INFO", tList));
                        tList = new List<Tuple<string, string>>();
                    }
                    continue;
                }
                else if (obj.Item1.Equals("V"))
                {
                    if (tList.Count() >= 1)
                    {
                        tList.Add(obj);
                        result.Add(new Tuple<string, List<Tuple<string, string>>>(string.Format("{0}{1:00}", Prefix, nIdx++), tList));
                        tList = new List<Tuple<string, string>>();
                    }
                    continue;
                }

                tList.Add(obj);
            }

            return result;
        }

        public bool LoadSpeedTable()
        {
            if (!File.Exists(SpeedFilePath))
            {
                err_info.Add("[Error] Speed Table을 찾을 수 없습니다.");
                return false;
            }

            List<string> speedList = File.ReadLines(SpeedFilePath).ToList();
            foreach (var item in speedList)
            {
                if (item.Contains('#')) continue;

                string[] splitData = item.Split('\t');
                if (splitData.Count() >= 4)
                {
                    double thick = Convert.ToDouble(splitData[0]);
                    int angle = Convert.ToInt32(splitData[1]);
                    int normal_speed = Convert.ToInt32(splitData[2]);
                    int adjusted_speed = Convert.ToInt32(splitData[3]);

                    speedTable.Add(new Tuple<double, int>(thick, angle), new Tuple<int, int>(normal_speed, adjusted_speed));
                }
            }

            return true;
        }

        public bool LoadGenericFile()
        {
            if (!File.Exists(GenericFilePath))
            {
                err_info.Add("[Error] Generic File을 찾을 수 없습니다.");
                return false;
            }

            List<Tuple<string, List<Tuple<string, string>>>> genfile_context = new List<Tuple<string, List<Tuple<string, string>>>>();
            
            bool bStart = false;
            string sMode = "";            
            List<Tuple<string, string>> sData = null;
            
            //구분하는 기준 
            //GENERAL_DATA / PART_DATA / LABELTEXT_DATA / PART_INFORMATION / IDLE_DATA / MARKING_DATA / BURNING_DATA            
            File.ReadLines(GenericFilePath).ToList().ForEach(f =>  {
                List<string> tData = f.Split('=').ToList();

                if (tData.Count() == 1 && bStart == false)
                {
                    sMode = tData.First();
                    bStart = true;
                    sData = new List<Tuple<string, string>>();
                }
                else if (tData.Count() == 2 && bStart)
                {
                    sData.Add(new Tuple<string, string>(tData.First(), tData.Last()));
                }
                else if (tData.Count() == 1 && bStart)
                {
                    if (tData.First().Contains(sMode))
                    {
                        genfile_context.Add(new Tuple<string, List<Tuple<string, string>>>(sMode, sData));
                        bStart = false;
                        sMode = "";
                    }
                    else
                    {
                        sData.Add(new Tuple<string, string>("SUBTYPE", tData.First()));
                    }
                }
            });

            CPartInfo cPartInfo = null;
            CSegType seqType = null;
            CConInfo conInfo = null;
            CBevelData bevelInfo = null;
            string jsonStr = "";
            
            foreach (Tuple<string, List<Tuple<string, string>>> obj in genfile_context)
            {
                jsonStr = "";
                switch(obj.Item1) {
                    case GenericData.GENERAL_DATA:
                        jsonStr = CommonUtil.ListToJsonString(obj.Item2);
                        generalInfo = JsonConvert.DeserializeObject<CGeneralInfo>(jsonStr);
                        break;
                    case GenericData.PART_DATA:
                        break;
                    case GenericData.LABELTEXT_DATA:
                        jsonStr = CommonUtil.ListToJsonString(obj.Item2);
                        CTextData newText = JsonConvert.DeserializeObject<CTextData>(jsonStr);
                        textInfo.Add(newText);
                        break;
                    case GenericData.PART_INFORMATION:
                        jsonStr = CommonUtil.ListToJsonString(obj.Item2);
                        cPartInfo = JsonConvert.DeserializeObject<CPartInfo>(jsonStr);                        
                        break;
                    case GenericData.IDLE_DATA:
                        List<Tuple<string, List<Tuple<string, string>>>> tList_idle = ListToDetail(obj.Item2);
                        conInfo = null;
                        seqType = null;
                        bevelInfo = null;
                        foreach(Tuple<string, List<Tuple<string, string>>> tObj in tList_idle) {
                            if (tObj.Item1.Equals("CON_INFO"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);
                                seqType = new CSegType();
                                seqType.data_type = Enum.CONTOUR_TYPE.IDLE_DATA;
                                conInfo = JsonConvert.DeserializeObject<CConInfo>(jsonStr);                                
                            }
                            else if (tObj.Item1.Contains("SEG"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);
                                CSegInfo newSegInfo = JsonConvert.DeserializeObject<CSegInfo>(jsonStr);
                                if (conInfo.seqList == null)
                                {
                                    conInfo.seqList = new List<CSegInfo>();
                                }
                                conInfo.seqList.Add(newSegInfo);
                            }
                        }
                        this.partInfo.Add(new Tuple<CPartInfo, CSegType, CConInfo>(cPartInfo, seqType, conInfo));
                        break;
                    case GenericData.MARKING_DATA:
                        List<Tuple<string, List<Tuple<string, string>>>> tList_marking = ListToDetail(obj.Item2);
                        conInfo = null;
                        seqType = null;
                        bevelInfo = null;
                        foreach (Tuple<string, List<Tuple<string, string>>> tObj in tList_marking)
                        {
                            if (tObj.Item1.Equals("INFORM"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);
                                seqType = JsonConvert.DeserializeObject<CDatal_Marking>(jsonStr);
                                seqType.data_type = Enum.CONTOUR_TYPE.MARKING_DATA;
                            }
                            else if (tObj.Item1.Equals("CON_INFO"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);                                                                
                                conInfo = JsonConvert.DeserializeObject<CConInfo>(jsonStr);                                
                            }
                            else if (tObj.Item1.Contains("SEG"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);
                                CSegInfo newSegInfo = JsonConvert.DeserializeObject<CSegInfo>(jsonStr);
                                if (conInfo.seqList == null)
                                {
                                    conInfo.seqList = new List<CSegInfo>();
                                }
                                conInfo.seqList.Add(newSegInfo);
                            }
                        }
                        this.partInfo.Add(new Tuple<CPartInfo, CSegType, CConInfo>(cPartInfo, seqType, conInfo));
                        break;
                    case GenericData.BURNING_DATA:
                        List<Tuple<string, List<Tuple<string, string>>>> tList_burning = ListToDetail(obj.Item2);
                        conInfo = null;
                        seqType = null;
                        bevelInfo = null;
                        foreach (Tuple<string, List<Tuple<string, string>>> tObj in tList_burning)
                        {
                            if (tObj.Item1.Equals("INFORM"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);
                                seqType = JsonConvert.DeserializeObject<CData_Burning>(jsonStr);
                                seqType.data_type = Enum.CONTOUR_TYPE.BURNING_DATA;
                            }
                            else if (tObj.Item1.Equals("CON_INFO"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);
                                conInfo = JsonConvert.DeserializeObject<CConInfo>(jsonStr);                                
                            }
                            else if (tObj.Item1.Equals("BEVEL_DATA"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);
                                bevelInfo = JsonConvert.DeserializeObject<CBevelData>(jsonStr);
                            }
                            else if (tObj.Item1.Contains("SEG"))
                            {
                                jsonStr = CommonUtil.ListToJsonString(tObj.Item2);
                                CSegInfo newSegInfo = JsonConvert.DeserializeObject<CSegInfo>(jsonStr);
                                if (conInfo.seqList == null)
                                {
                                    conInfo.seqList = new List<CSegInfo>();
                                }
                                conInfo.seqList.Add(newSegInfo);
                            }
                        }
                        conInfo.bevel_data = bevelInfo;
                        this.partInfo.Add(new Tuple<CPartInfo, CSegType, CConInfo>(cPartInfo,seqType,conInfo));
                        break;
                }
            }

            return true;
        }

        public void SetGeoList()
        {
            NCType NcType = NCType.IDLE;
            NCSubType SubType = NCSubType.NONE;
            for (int nIdx = 0; nIdx < partInfo.Count(); nIdx++)
            {
                var obj = partInfo[nIdx];
                if (obj.Item2.data_type == Enum.CONTOUR_TYPE.IDLE_DATA)
                {
                    NcType = NCType.IDLE;
                    SubType = NCSubType.NONE;
                    if (obj.Item3.seqList.Count() <= 1)
                    {
                        CSegInfo seg = obj.Item3.seqList.First();
                        CLine newLine = new CLine();
                        newLine.Type = NcType;
                        newLine.SubType = SubType;
                        newLine.StartPoint = new CPoint(obj.Item3.start_u, obj.Item3.start_v);
                        newLine.EndPoint = new CPoint(seg.u, seg.v);
                        newLine.Radius = seg.radius * (seg.amp > 0 ? 1 : -1);
                        newLine.CenterPoint = new CPoint(seg.origin_u, seg.origin_v);
                        newLine.LineType = seg.amp <= 0.001 ? LineType.Line : LineType.Arc;
                        geo.Add(newLine);
                    }
                    else
                    {
                        for (int i = 0; i < obj.Item3.seqList.Count(); i++)
                        {
                            CSegInfo Seg = obj.Item3.seqList[i];                            
                            CLine newLine = new CLine();
                            if (i == 0)
                            {
                                newLine.Type = NcType;
                                newLine.SubType = SubType;
                                newLine.StartPoint = new CPoint(obj.Item3.start_u, obj.Item3.start_v);
                                newLine.EndPoint = new CPoint(Seg.u, Seg.v);
                                newLine.Radius = Seg.radius * (Seg.amp > 0 ? 1 : -1);
                                newLine.CenterPoint = new CPoint(Seg.origin_u, Seg.origin_v);
                                newLine.LineType = Seg.amp <= 0.001 ? LineType.Line : LineType.Arc;
                                geo.Add(newLine);
                            }
                            else
                            {
                                CSegInfo Pre_Seg = obj.Item3.seqList[i - 1];
                                newLine.Type = NcType;
                                newLine.SubType = SubType;
                                newLine.StartPoint = new CPoint(Pre_Seg.u, Pre_Seg.v);
                                newLine.EndPoint = new CPoint(Seg.u, Seg.v);
                                newLine.Radius = Seg.radius * (Seg.amp > 0 ? 1 : -1);
                                newLine.CenterPoint = new CPoint(Seg.origin_u, Seg.origin_v);
                                newLine.LineType = Seg.amp <= 0.001 ? LineType.Line : LineType.Arc;
                                geo.Add(newLine);
                            }
                        }
                    }
                }
                else if (obj.Item2.data_type == Enum.CONTOUR_TYPE.MARKING_DATA)
                {
                    NcType = NCType.MARKING;
                    SubType = NCSubType.NONE;
                    if (obj.Item3.seqList.Count() <= 1)
                    {
                        CSegInfo seg = obj.Item3.seqList.First();
                        CLine newLine = new CLine();
                        newLine.Type = NcType;
                        newLine.SubType = SubType;
                        newLine.StartPoint = new CPoint(obj.Item3.start_u, obj.Item3.start_v);
                        newLine.EndPoint = new CPoint(seg.u, seg.v);
                        newLine.Radius = seg.radius * (seg.amp > 0 ? 1 : -1);
                        newLine.CenterPoint = new CPoint(seg.origin_u, seg.origin_v);
                        newLine.LineType = Math.Abs(seg.amp) <= 0.001 ? LineType.Line : LineType.Arc;
                        geo.Add(newLine);
                    }
                    else
                    {
                        for (int i = 0; i < obj.Item3.seqList.Count(); i++)
                        {
                            CSegInfo Seg = obj.Item3.seqList[i];                            
                            CLine newLine = new CLine();
                            if (i == 0)
                            {
                                newLine.Type = NcType;
                                newLine.SubType = SubType;
                                newLine.StartPoint = new CPoint(obj.Item3.start_u, obj.Item3.start_v);
                                newLine.EndPoint = new CPoint(Seg.u, Seg.v);
                                newLine.Radius = Seg.radius * (Seg.amp > 0 ? 1 : -1);
                                newLine.CenterPoint = new CPoint(Seg.origin_u, Seg.origin_v);
                                newLine.LineType = Math.Abs(Seg.amp) <= 0.001 ? LineType.Line : LineType.Arc;
                                geo.Add(newLine);
                            }
                            else
                            {
                                CSegInfo Pre_Seg = obj.Item3.seqList[i - 1];
                                newLine.Type = NcType;
                                newLine.SubType = SubType;
                                newLine.StartPoint = new CPoint(Pre_Seg.u, Pre_Seg.v);
                                newLine.EndPoint = new CPoint(Seg.u, Seg.v);
                                newLine.Radius = Seg.radius * (Seg.amp > 0 ? 1 : -1);
                                newLine.CenterPoint = new CPoint(Seg.origin_u, Seg.origin_v);
                                newLine.LineType = Math.Abs(Seg.amp) <= 0.001 ? LineType.Line : LineType.Arc;
                                geo.Add(newLine);
                            }
                        }
                    }

                }
                else if (obj.Item2.data_type == Enum.CONTOUR_TYPE.BURNING_DATA)
                {
                    CData_Burning tObj = (CData_Burning)obj.Item2;
                    if (tObj.shape.Equals(DefineProperty.OUTER_CONTOUR))
                    {
                        NcType = NCType.OUT;
                        SubType = NCSubType.CUT;
                    }
                    else if (tObj.shape.Equals(DefineProperty.HOLE))
                    {
                        NcType = NCType.IN;
                        SubType = NCSubType.CUT;
                    }
                    else if (tObj.shape.Equals(DefineProperty.START_HOOK))
                    {
                        SubType = NCSubType.LEAD_IN;
                        if ((partInfo[nIdx+1].Item2 as CData_Burning).shape.Equals(DefineProperty.OUTER_CONTOUR))
                        {
                            NcType = NCType.OUT;
                        }
                        else
                        {
                            NcType = NCType.IN;
                        }
                    }
                    else if (tObj.shape.Equals(DefineProperty.END_HOOK))
                    {
                        SubType = NCSubType.LEAD_OUT;
                        if ((partInfo[nIdx - 1].Item2 as CData_Burning).shape.Equals(DefineProperty.OUTER_CONTOUR))
                        {
                            NcType = NCType.OUT;
                        }
                        else
                        {
                            NcType = NCType.IN;
                        }
                    }
                    else if (tObj.shape.Equals(DefineProperty.CORNER_LOOP))
                    {
                        SubType = NCSubType.CONER_LOOP;
                        NcType = NCType.OUT;
                    }

                    if (obj.Item3.seqList.Count() <= 1)
                    {
                        CSegInfo seg = obj.Item3.seqList.First();
                        CLine newLine = new CLine();
                        newLine.Type = NcType;
                        newLine.SubType = SubType;
                        newLine.StartPoint = new CPoint(obj.Item3.start_u, obj.Item3.start_v);
                        newLine.EndPoint = new CPoint(seg.u, seg.v);
                        newLine.Radius = seg.radius * (seg.amp > 0 ? 1 : -1);
                        newLine.CenterPoint = new CPoint(seg.origin_u, seg.origin_v);
                        newLine.LineType = Math.Abs(seg.amp) <= 0.001 ? LineType.Line : LineType.Arc;
                        
                        if (obj.Item3.bevel_data.angle_ts > 0) newLine.Bevel = obj.Item3.bevel_data.angle_ts;
                        else if (obj.Item3.bevel_data.angle_os > 0) newLine.Bevel = -obj.Item3.bevel_data.angle_os;

                        geo.Add(newLine);
                    }
                    else
                    {
                        for (int i = 0; i < obj.Item3.seqList.Count(); i++)
                        {
                            CSegInfo Seg = obj.Item3.seqList[i];                            
                            CLine newLine = new CLine();
                            if (i == 0)
                            {
                                newLine.Type = NcType;
                                newLine.SubType = SubType;
                                newLine.StartPoint = new CPoint(obj.Item3.start_u, obj.Item3.start_v);
                                newLine.EndPoint = new CPoint(Seg.u, Seg.v);
                                newLine.Radius = Seg.radius * (Seg.amp > 0 ? 1 : -1);
                                newLine.CenterPoint = new CPoint(Seg.origin_u, Seg.origin_v);
                                newLine.LineType = Math.Abs(Seg.amp) <= 0.001 ? LineType.Line : LineType.Arc;

                                if (obj.Item3.bevel_data.angle_ts > 0) newLine.Bevel = obj.Item3.bevel_data.angle_ts;
                                else if (obj.Item3.bevel_data.angle_os > 0) newLine.Bevel = -obj.Item3.bevel_data.angle_os;

                                geo.Add(newLine);
                            }
                            else
                            {
                                CSegInfo Pre_Seg = obj.Item3.seqList[i - 1];
                                newLine.Type = NcType;
                                newLine.SubType = SubType;
                                newLine.StartPoint = new CPoint(Pre_Seg.u, Pre_Seg.v);
                                newLine.EndPoint = new CPoint(Seg.u, Seg.v);
                                newLine.Radius = Seg.radius * (Seg.amp > 0 ? 1 : -1);
                                newLine.CenterPoint = new CPoint(Seg.origin_u, Seg.origin_v);
                                newLine.LineType = Math.Abs(Seg.amp) <= 0.001 ? LineType.Line : LineType.Arc;

                                if (obj.Item3.bevel_data.angle_ts > 0) newLine.Bevel = obj.Item3.bevel_data.angle_ts;
                                else if (obj.Item3.bevel_data.angle_os > 0) newLine.Bevel = -obj.Item3.bevel_data.angle_os;

                                geo.Add(newLine);
                            }
                        }
                    }

                }
            }
            // 마킹 삭제
            if (!bWriteMarking)
            {
                List<CLine> geoTemp = new List<CLine>();
                
                for (int i=0; i < geo.Count(); i++)
                {
                    NcType = geo[i].Type;
                    SubType = geo[i].SubType;

                    if (NcType == NCType.IN || NcType == NCType.OUT)
                    {
                        geoTemp.AddRange(geo.GetRange(i-1, geo.Count() - (i-1)));
                        // 시작점을  0,0 으로 변경
                        geoTemp.First().StartPoint.X = 0;
                        geoTemp.First().StartPoint.Y = 0;
                        break;
                    }
                }
                geo.Clear();
                geo.AddRange(geoTemp);
                geoTemp.Clear();

            }

            // bevel 삭제
            if (!bWriteBevel)
            {
                List<CLine> geoTemp = new List<CLine>();

                for (int i = 0; i < geo.Count(); i++)
                {
                    NcType = geo[i].Type;
                    SubType = geo[i].SubType;
                    
                    if (SubType == NCSubType.CONER_LOOP) continue;
                    CLine cLine = geo[i].Clone();
                    cLine.Bevel = 0;

                    geoTemp.Add(cLine);
                }

                geo.Clear();
                geo.AddRange(geoTemp);
                geoTemp.Clear();

            }

            // 텍스트 라벨 소팅
            textInfo = textInfo.OrderBy(w => Convert.ToInt32(w.position_u) / 1000).ThenBy(w => w.position_v).ToList();
            //textInfo = textInfo.OrderBy(w => w.position_u / 1000).ToList();
            
        }


        public bool ConvertEssiCode()
        {
            int cornerLoopCount = 0;

            cornerLoopCount = GetCornerLoopCount(3);
            if (cornerLoopCount == -1)
            {
                err_info.Add("[Error] Generic File 의 corner loop 개수가 3개로 일정하지 않습니다.");
                return false;
            }
            // corner loop 3번을 두개로 쪼갠다.
            ChangeGeometry_CornerLoop(3);

            cornerLoopCount = GetCornerLoopCount(4);
            if (cornerLoopCount == -1)
            {
                err_info.Add("[Error] corner loop 변환 후 corner loop 개수가 4개로 일정하지 않습니다.");
                return false;
            }
            // foot sensor 를 삽입한다.
            ChangeGeometry_FootSensor(DefineValue.FootSensorDist);

            NCType curType = NCType.IDLE;
            NCSubType curSubType = NCSubType.NONE;
            LineType curLineType = LineType.Line;

            bMarkingOffsetOff = true; // 마킹 상태 Rapid 5,6 조절하기 위해

            double curRadius = 0;
            double curBevel = 0;
            double curTorchX = 0;
            double curTorchY = 0;

            int curIdleQty = 0;
            int totalIdleQty = 0;
            int curMarkQty = 0;
            int curLabelMarkQty = 0; // label mark qty
            int totalMarkQty = 0;
            int curCutQty = 0;
            int totalCutQty = 0;
            int curLeadinQty = 0;
            int curLeadoutQty = 0;
            int curCornerQty = 0;

            int index = 0;

            WriteCode_Start();
            // header

            WriteCode_Speed(0, NCSpeed.NORMAL);
            
            //if (MachinePackage == Machine.TBC)
            //{
            //    mainCode.Add("32");
            //    mainCode.Add("31+24");
            //}
            //else 
            //{ // TBP
            //    mainCode.Add("32");
            //    mainCode.Add("31+2");
            //}

            if (bWriteText)
            {
                curLabelMarkQty = WriteCode_LabelText();

                geo[0].StartPoint.X = textInfo.Last().position_u;
                geo[0].StartPoint.Y = textInfo.Last().position_v;
            }

            for (index = 0; index < geo.Count(); index++)
            {
                curType = geo[index].Type;
                curSubType = geo[index].SubType;
                curRadius = geo[index].Radius;
                curBevel = geo[index].Bevel;
                curLineType = geo[index].LineType;
                curTorchX = geo[index].EndPoint.X;
                curTorchY = geo[index].EndPoint.Y;

                if (curType == NCType.IDLE)
                {
                    curIdleQty++;
                    totalIdleQty++;

                    if (curCutQty > 0) WriteCode_CuttingOff(geo[index - 1]);
                    if (curMarkQty > 0)
                    {
                        WriteCode_MarkingOff(MachinePackage);
                        if (geo[index + 1].Type != NCType.MARKING)
                        {
                            WriteCode_MarkingOffsetOff(MachinePackage);
                            bMarkingOffsetOff = true;
                        }
                    }
                    else if (curLabelMarkQty > 0)
                    {
                        if (geo[index + 1].Type != NCType.MARKING)
                        {
                            WriteCode_MarkingOffsetOff(MachinePackage);
                            bMarkingOffsetOff = true;
                        }
                    }

                    curCutQty = curMarkQty = curLabelMarkQty = 0;
                    curLeadinQty = curLeadoutQty = curCornerQty = 0;

                    if (index + 1 == geo.Count()) continue;

                    // marking offset off 가 yes(true) 인 구간에서 Rapid 5,6을 사용한다.
                    // 1번 마킹하러 갈때, 1번 커팅하러 갈 때와 그 이후 주욱, 
                    if (bMarkingOffsetOff) WriteCode_RapidOn();
                    WriteCode_Geo(geo[index]);
                    if (bMarkingOffsetOff) WriteCode_RapidOff();

                    continue;
                }
                else if (curType == NCType.MARKING)
                {
                    curMarkQty++;
                    totalMarkQty++;

                    // label text 를 사용한 경우 bMarkingOffsetOff 가 no(false) 되어 있다.
                    if (totalMarkQty == 1 && bMarkingOffsetOff)
                    {
                        WriteCode_MarkingOffsetOn(MachinePackage);
                        bMarkingOffsetOff = false;
                    }
                    if (curMarkQty == 1) WriteCode_MarkingOn(MachinePackage);

                    curIdleQty = 0;
                    curCutQty = 0;
                    totalCutQty = 0;
                }
                else if (curType == NCType.OUT || curType == NCType.IN)
                {
                    curCutQty++;
                    totalCutQty++;
                    if (curSubType == NCSubType.LEAD_IN)
                    {
                        curLeadinQty++;
                        if (curLeadinQty == 1) WriteCode_CuttingOn(geo[index]);
                    }
                    else if (curSubType == NCSubType.LEAD_OUT)
                    {
                        curLeadoutQty++;
                        if (curBevel == 0)
                        {
                            mainCode.Add("48");
                        }
                    }
                    else if (curSubType == NCSubType.FOOTSENSOR)
                    {
                        mainCode.Add("48");
                        mainCode.Add("77");
                    }
                    else if (curSubType == NCSubType.CONER_LOOP)
                    {
                        double startAngle = geo[index - 1 - curCornerQty].Bevel;
                        double endAngle = geo[index + cornerLoopCount - curCornerQty].Bevel;

                        curCornerQty++;

                        if (curCornerQty == 1)
                        {
                            if (startAngle == 0 && endAngle != 0)
                            {
                                mainCode.Add("48");
                                WriteCode_Speed(startAngle, NCSpeed.ADJUSTED);
                            }
                            if (startAngle != 0 && endAngle == 0)
                            {
                                WriteCode_Speed(startAngle, NCSpeed.ADJUSTED);
                            }
                            if (startAngle != 0 && endAngle != 0)
                            {
                                WriteCode_Speed(startAngle, NCSpeed.ADJUSTED);
                            }
                        }
                        else if (curCornerQty == 2)
                        {
                            if (startAngle == 0 && endAngle != 0)
                            {
                                if (!bTangentialControl)
                                {
                                    mainCode.Add("51");
                                    bTangentialControl = true;
                                }
                                
                                mainCode.Add("118");
                                mainCode.Add(Code_Bevel(endAngle));
                            }
                            if (startAngle != 0 && endAngle == 0)
                            {
                                mainCode.Add("118");
                                mainCode.Add("122+0");
                            }
                            if (startAngle != 0 && endAngle != 0)
                            {
                                mainCode.Add("118");
                                mainCode.Add(Code_Bevel(endAngle));
                            }
                        }
                        else if (curCornerQty == cornerLoopCount - 1)
                        {
                            if (startAngle == 0 && endAngle != 0)
                            {
                                mainCode.Add("76");
                            }
                            if (startAngle != 0 && endAngle == 0)
                            {

                            }
                            if (startAngle != 0 && endAngle != 0)
                            {
                                mainCode.Add("76");
                            }
                        }
                        else if (curCornerQty == cornerLoopCount)
                        {
                            if (startAngle == 0 && endAngle != 0)
                            {
                                mainCode.Add("47");
                                WriteCode_Speed(endAngle, NCSpeed.NORMAL);
                            }
                            if (startAngle != 0 && endAngle == 0)
                            {
                                mainCode.Add("47");
                                WriteCode_Speed(endAngle, NCSpeed.NORMAL);
                            }
                            if (startAngle != 0 && endAngle != 0)
                            {
                                mainCode.Add("47");
                                WriteCode_Speed(endAngle, NCSpeed.NORMAL);
                            }

                            curCornerQty = 0;
                        }

                    }
                    else
                    {
                        curLeadinQty = curLeadoutQty = curCornerQty = 0;
                        
                        // 2016.05.09 AUTOBEVEL 코드 삽입
                        if (geo[index - 1].Type == NCType.OUT && geo[index -1].SubType == NCSubType.CUT)
                        {
                            if (geo[index - 1].Bevel != geo[index].Bevel)
                            {
                                mainCode.Add(Code_Bevel(geo[index].Bevel));
                                WriteCode_Speed(geo[index].Bevel, NCSpeed.NORMAL);
                            }
                        }
                    }

                    curMarkQty = curIdleQty = 0;
                }

                WriteCode_Geo(geo[index]);
            }


            if (totalMarkQty > 0)
            {
                if (curMarkQty > 0) WriteCode_MarkingOff();
            }
            if (totalCutQty > 0)
            {
                if (curCutQty > 0) WriteCode_CuttingOff(geo[index - 1]);
            }

            WriteCode_End();

            System.IO.File.WriteAllLines(EssiFilePath, mainCode);

            return true;
        }

        public bool ChangeGeometry_CornerLoop(int changeCornerNumber)
        {
            bool bRtn = true;

            List<CLine> geoTemp = new List<CLine>();
            
            int cornerCount = 0;

            for (int i = 0; i < geo.Count(); i++)
            {
                NCType type = geo[i].Type;
                NCSubType subType = geo[i].SubType;
                geoTemp.Add(geo[i].Clone());

                if (subType == NCSubType.CONER_LOOP)
                {
                    cornerCount++;
                    if (cornerCount == changeCornerNumber)
                    {
                        CLine cLine = geo[i];
                        CPoint midPoint = MathUtil.cal_LineDistancePt(cLine.StartPoint, cLine.EndPoint, MathUtil.GetDistance(cLine.StartPoint, cLine.EndPoint) / 2.0);
                        geoTemp.Last().EndPoint.X = midPoint.X;
                        geoTemp.Last().EndPoint.Y = midPoint.Y;

                        geoTemp.Add(geo[i].Clone());
                        geoTemp.Last().StartPoint.X = midPoint.X;
                        geoTemp.Last().StartPoint.Y = midPoint.Y;
                    }
                }
                else
                {
                    cornerCount = 0;
                }
            }

            geo.Clear();
            foreach(var item in geoTemp)
            {
                geo.Add(item.Clone());
            }
            geoTemp.Clear();

            return bRtn;
        }

        List<CLine> FindOutFootSensor(List<CLine> geoList, int startIndex, out int outIndex, double breakLength)
        {
            List<CLine> newList = new List<CLine>();
            Dictionary<int, double> lenDict = new Dictionary<int, double>();
            // index no
            // startIndex -- breakIndex -- outIndex(corner or leadout)
            // 
            outIndex = startIndex + 1;
            int breakIndex = startIndex;

            for (int i = startIndex; i < geoList.Count(); i++)
            {
                CLine cLine = geoList[i];
                NCType type = geoList[i].Type;
                NCSubType subType = geoList[i].SubType;

                if (subType == NCSubType.CUT)
                {
                    double len;
                    if (cLine.LineType == LineType.Line) 
                        len = MathUtil.cal_LineLen(cLine.StartPoint, cLine.EndPoint);
                    else
                        len = MathUtil.cal_ArcLen(cLine.CenterPoint, cLine.Radius, cLine.StartPoint, cLine.EndPoint);
                    
                    lenDict.Add(i, len);
                }
                else if (subType == NCSubType.CONER_LOOP || subType == NCSubType.LEAD_OUT)
                {
                    outIndex = i;
                    break;
                }
            }

            double totalLen = lenDict.Sum(s => s.Value);
            double limitLen;
            // 길이가 60 이하 일때
            if (totalLen <= breakLength)
            {
                //limitLen = breakLength / 2.0; // 20160714 cgkim 수정
                limitLen = totalLen / 2.0;
            }
            else
            {
                limitLen = lenDict.Sum(s => s.Value) - breakLength;
            }
            
            double curLen = 0;
            double preLen = 0;
            CPoint breakPoint = new CPoint();
            
            foreach (var item in lenDict)
            {
                curLen += item.Value;
                if (curLen > limitLen)
                {
                    breakIndex = item.Key;

                    if (geoList[item.Key].LineType == LineType.Line)
                        breakPoint = MathUtil.cal_LineDistancePt(geoList[item.Key].StartPoint, geoList[item.Key].EndPoint, limitLen - preLen);
                    else
                        breakPoint = MathUtil.cal_ArcDistancePt(geoList[item.Key].StartPoint, geoList[item.Key].EndPoint, geoList[item.Key].CenterPoint, geoList[item.Key].Radius, limitLen - preLen);

                    break;
                }
                preLen = curLen;
            }

            newList.AddRange(geoList.GetRange(startIndex, breakIndex - startIndex));

            newList.Add(geoList[breakIndex].Clone());
            newList.Last().EndPoint.X = breakPoint.X;
            newList.Last().EndPoint.Y = breakPoint.Y;

            newList.Add(geoList[breakIndex].Clone());
            newList.Last().StartPoint.X = breakPoint.X;
            newList.Last().StartPoint.Y = breakPoint.Y;
            newList.Last().SubType = NCSubType.FOOTSENSOR;

            newList.AddRange(geoList.GetRange(breakIndex + 1, outIndex - (breakIndex + 1)));

            return newList;
        }

        /// <summary>
        /// 개선절단이 있는 변을 대상
        /// 코너루프 진입 직전 60mm 되는 곳에 48, 77 명령을 삽입하기 위해 geo를 쪼갠다.
        /// 
        /// </summary>
        /// <param name="breakLength"></param>
        public void ChangeGeometry_FootSensor(double breakLength)
        {
            List<CLine> geoTemp = new List<CLine>();
            int outIndex;
            int ndx = 0;

            while (ndx < geo.Count())
            {
                NCType type = geo[ndx].Type;
                NCSubType subType = geo[ndx].SubType;

                geoTemp.Add(geo[ndx].Clone());

                if (subType == NCSubType.LEAD_IN || subType == NCSubType.CONER_LOOP)
                {
                    if (geo[ndx + 1].SubType == NCSubType.CUT && Math.Abs(geo[ndx + 1].Bevel) > 0)
                    {
                        // 본절단선 부터 본 절단선 까지
                        List<CLine> outList = FindOutFootSensor(geo, ndx + 1, out outIndex, breakLength);
                        geoTemp.AddRange(outList);

                        ndx = outIndex;
                        continue;
                    }
                }
                ndx++;
            }

            geo.Clear();
            geo.AddRange(geoTemp);
            
            geoTemp.Clear();
            
        }

        public int GetCornerLoopCount(int nLimit)
        {
            int cnt = 0;
            List<int> cornerCountList = new List<int>();

            for (int i =0; i < geo.Count(); i++)
            {
                if (geo[i].Type == NCType.OUT && geo[i].SubType == NCSubType.CONER_LOOP) cnt++;
                else
                {
                    if (cnt > 0)
                    {
                        cornerCountList.Add(cnt);
                        cnt = 0;
                    }
                }
            }
            
            if (cornerCountList.Count == 0) return 0;
            else if (cornerCountList.Where(w => !w.Equals(nLimit)).Count() >= 1)
            {
                return -1;
            }
            else {
                return nLimit;
            }
        }

        public string Code_Bevel(double bevelAngle)
        {
            string aCode = "122+0";
            aCode = string.Format("122{0:+#;-#;+0}", System.Convert.ToInt32(bevelAngle * 10.0));
            return aCode;
        }

        /// <summary>
        /// write dump all geo
        /// </summary>
        public void WriteCode_Debug()
        {
            string aCode = "";

            for (int i = 0; i < geo.Count(); i++)
            {
                NCType type = geo[i].Type;
                NCSubType subType = geo[i].SubType;
                CPoint startPoint = geo[i].StartPoint;
                CPoint endPoint = geo[i].EndPoint;
                CPoint centerPoint = geo[i].CenterPoint;
                double radius = geo[i].Radius;
                double bevel = geo[i].Bevel;

                aCode = string.Format("{0,4}   {1,-15}{2,-15}{3,12:F1}{4,12:F1}{5,12:F1}{6,12:F1}{7,12:F1}{8,12:F1}{9,12:F1}{10,12:F1}",
                    i, type, subType, startPoint.X, startPoint.Y, endPoint.X, endPoint.Y, centerPoint.X, centerPoint.Y, radius, bevel);

                mainCode.Add(aCode);
            }

        }

        public int WriteCode_LabelText()
        {
            string aCode = "";
            double curX = 0;
            double curY = 0;
            double preX = 0;
            double preY = 0;

            for (int i = 0; i < textInfo.Count(); i++)
            {
                curX = textInfo[i].position_u;
                curY = textInfo[i].position_v;

                int delX = System.Convert.ToInt32((curX - preX) * 10.0);
                int delY = System.Convert.ToInt32((curY - preY) * 10.0);

                if (i == 0)
                {
                    //mainCode.Add("5");
                    WriteCode_RapidOn();
                    aCode = string.Format("{0:+#;-#;+0}{1:+#;-#;+0}", delX, delY);
                    mainCode.Add(aCode);
                    //mainCode.Add("6");
                    WriteCode_RapidOff();
                    
                    //mainCode.Add("32");
                    //mainCode.Add("31+1");
                    
                    //mainCode.Add("114");
                    //mainCode.Add("45");
                    WriteCode_MarkingOffsetOn();
                    bMarkingOffsetOff = false;
                }
                else
                {
                    if (delX != 0 || delY != 0)
                    {
                        aCode = string.Format("{0:+#;-#;+0}{1:+#;-#;+0}", delX, delY);
                        mainCode.Add(aCode);
                    }
                }
                mainCode.Add("223+2");

                // text height
                int textHeight = System.Convert.ToInt32(Math.Abs(textInfo[i].height));
                
                if (!textHeight.Equals(2) && !textHeight.Equals(4)  && !textHeight.Equals(6) )
                {
                    textHeight = 4; // default
                }
                aCode = string.Format("225+{0}", textHeight);
                mainCode.Add(aCode);
                
                // text angle
                int textAngle = System.Convert.ToInt32(MathUtil.RadianToDegree(textInfo[i].angle)) * 100;
                aCode = string.Format("226{0:+#;-#;+0}", textAngle);
                mainCode.Add(aCode);

                mainCode.Add("224");
                aCode = string.Format("{0}", textInfo[i].text);
                mainCode.Add(aCode);
                mainCode.Add("4");
                mainCode.Add("206");
                mainCode.Add("111");

                preX = curX;
                preY = curY;
            }

            return textInfo.Count();
        }

        /// <summary>
        /// code function
        /// </summary>
        public void WriteCode_Start()
        {
            string aCode1 = "";
            string aCode2 = "";
            int plateQuantity = 1;
            
            if (CuttingOptionPackage == CuttingOption.C)
            {
                mainCode.Add("32");
                mainCode.Add("31+2");
            }
            else if (CuttingOptionPackage == CuttingOption.CC)
            {
                plateQuantity = 2;
                mainCode.Add("CUTTING TWO EQUAL PLATES (Y/N) ?");
                mainCode.Add("32");
                mainCode.Add("31+24");
            }
            else if (CuttingOptionPackage == CuttingOption.CM)
            {
                mainCode.Add("CUTTING TWO PLATES IDENTICAL-SYMMETRICAL (Y/N) ?");
                mainCode.Add("32");
                mainCode.Add("31+24");
            }

            mainCode.Add("3");

            //B4010086.MPG
            FileInfo fileInfo = new FileInfo(EssiFilePath);
            mainCode.Add(string.Format("{0}", fileInfo.Name));

            //12345678901234567890123456789012345678901234567890
            //C505/B401/0086  1X  450.X 650./12.0 AH  
            //C505/B401/0063  1X 2500.X2000./ 7.0 AH  
            aCode1 = string.Format("{0}/{1}/{2}", generalInfo.nest_name.Substring(0, 4), generalInfo.nest_name.Substring(4, 5), generalInfo.nest_name.Substring(9));
            aCode2 = string.Format("{0}X{1,5}.X{2,4}./{3,4:F1} {4,-4}", plateQuantity, Convert.ToInt32(generalInfo.raw_length), Convert.ToInt32(generalInfo.raw_width), generalInfo.raw_thickness, generalInfo.quality);
            mainCode.Add(aCode1+aCode2);
            
            mainCode.Add("4");

            if (CuttingOptionPackage == CuttingOption.C)
            {
            }
            else if (CuttingOptionPackage == CuttingOption.CC)
            {
                mainCode.Add("19+");
            }
            else if (CuttingOptionPackage == CuttingOption.CM)
            {
                mainCode.Add("19-");
            }
        }

        public void WriteCode_End()
        {
            mainCode.Add("32");
            mainCode.Add("46");
            mainCode.Add("63");
        }

        public void WriteCode_RapidOn()
        {
            mainCode.Add("5");
        }
        public void WriteCode_RapidOff()
        {
            mainCode.Add("6");
        }

        public void WriteCode_MarkingOn(Machine machine = Machine.None)
        {
            mainCode.Add("110");
        }
        public void WriteCode_MarkingOff(Machine machine = Machine.None)
        {
            mainCode.Add("111");
        }

        public void WriteCode_MarkingOffsetOn(Machine machine = Machine.None)
        {
            if (CuttingOptionPackage == CuttingOption.C)
            {
                mainCode.Add("32");
                mainCode.Add("31+1");
            }
            else if (CuttingOptionPackage == CuttingOption.CC)
            {
                mainCode.Add("32");
                mainCode.Add("31+13");
            }
            else if (CuttingOptionPackage == CuttingOption.CM)
            {
                mainCode.Add("32");
                mainCode.Add("31+13");
            }

            //if (machine == Machine.TBC)
            //{
            //    mainCode.Add("32");
            //    mainCode.Add("31+13");
            //}
            
            mainCode.Add("114");
            mainCode.Add("45");
        }
        public void WriteCode_MarkingOffsetOff(Machine machine = Machine.None)
        {
            mainCode.Add("46");
            mainCode.Add("12");
            //if (machine == Machine.TBC)
            //{
            //    mainCode.Add("32");
            //    mainCode.Add("31+24");
            //}
            //else
            //{
            //    mainCode.Add("32");
            //    mainCode.Add("31+2");
            //}
            if (CuttingOptionPackage == CuttingOption.C)
            {
                mainCode.Add("32");
                mainCode.Add("31+2");
            }
            else if (CuttingOptionPackage == CuttingOption.CC)
            {
                mainCode.Add("32");
                mainCode.Add("31+24");
            }
            else if (CuttingOptionPackage == CuttingOption.CM)
            {
                mainCode.Add("32");
                mainCode.Add("31+24");
            }
            
            mainCode.Add("0");
        }

        public void WriteCode_Speed(double bevelAngle, NCSpeed ncSpeed = NCSpeed.NORMAL)
        {
            if (!bWriteSpeed) return;

            string aCode = "";
            double thickness = Convert.ToDouble(generalInfo.raw_thickness);

            // 2016.02.23 navantia로 부터 받은 speed table
            // angle 은 0도 밖에 없음.
            int angle = 0;

            Tuple<double, int> speedKey = new Tuple<double, int>(thickness, angle);

            if (speedTable.ContainsKey(speedKey))
            {
                Tuple<int, int> speedValue;
                if (speedTable.TryGetValue(speedKey, out speedValue))
                {
                    if (ncSpeed == NCSpeed.NORMAL)
                    {
                        aCode = string.Format("39+{0}", speedValue.Item1.ToString());
                    }
                    else
                    {
                        aCode = string.Format("39+{0}", speedValue.Item2.ToString());
                    }

                    mainCode.Add(aCode);
                }
                else
                {
                    err_info.Add(string.Format("[Error] TryGetValue return false / 두께:{0} 베벨:{1} ", generalInfo.raw_thickness, bevelAngle));

                }

            }
            else
            {
                err_info.Add(string.Format("[Error] 두께:{0} 베벨:{1} Speed 값을 찾을 수 없습니다.", generalInfo.raw_thickness, bevelAngle));
            }

        }

        public void WriteCode_CuttingOn(CLine geo)
        {
            if (Math.Abs(geo.Bevel) > 0)
            {
                mainCode.Add("76");
                mainCode.Add("30");
                mainCode.Add("51");
                mainCode.Add(Code_Bevel(geo.Bevel));
                mainCode.Add("53");

                bTangentialControl = true;
            }
            else
            {
                mainCode.Add("77");
                mainCode.Add("30");
                mainCode.Add("53");
            }
            
        }
        public void WriteCode_CuttingOff(CLine geo)
        {
            mainCode.Add("54");
            mainCode.Add("38");
            
            if (Math.Abs(geo.Bevel) > 0 || bTangentialControl)
            {
                mainCode.Add("122+0");
                mainCode.Add("52");

                bTangentialControl = false;
            }
        }

        /// <summary>
        /// Write one geo to code
        /// </summary>
        /// <param name="geo"></param>
        public void WriteCode_Geo(CLine geo)
        {
            string aCode = "";
            int delX = 0;
            int delY = 0;
            int delCx = 0;
            int delCy = 0;

            if (geo.LineType == LineType.Line)
            {
                delX = Convert.ToInt32((geo.EndPoint.X -  geo.StartPoint.X) * 10.0);
                delY = Convert.ToInt32((geo.EndPoint.Y -  geo.StartPoint.Y) * 10.0);
                aCode = string.Format("{0:+#;-#;+0}{1:+#;-#;+0}", delX, delY);
            }
            else
            {
                delX = Convert.ToInt32((geo.EndPoint.X - geo.StartPoint.X) * 10.0);
                delY = Convert.ToInt32((geo.EndPoint.Y - geo.StartPoint.Y) * 10.0);
                delCx = Convert.ToInt32((geo.CenterPoint.X - geo.StartPoint.X) * 10.0);
                delCy = Convert.ToInt32((geo.CenterPoint.Y - geo.StartPoint.Y) * 10.0);

                if (geo.Radius > 0)
                {
                    aCode = string.Format("{0:+#;-#;+0}{1:+#;-#;+0}{2:+#;-#;+0}{3:+#;-#;+0}+", delX, delY, delCx, delCy);
                }
                else
                {
                    aCode = string.Format("{0:+#;-#;+0}{1:+#;-#;+0}{2:+#;-#;+0}{3:+#;-#;+0}-", delX, delY, delCx, delCy);
                }

            }

            mainCode.Add(aCode);
        }
    
    

    }
}
