﻿using GenericClass.Define;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass
{
    public class CSegInfo
    {
        [JsonProperty(DefineProperty.AMP)]
        public double amp { get; set; }

        [JsonProperty(DefineProperty.AMP_U)]
        public double amp_u { get; set; }

        [JsonProperty(DefineProperty.AMP_V)]
        public double amp_v { get; set; }

        [JsonProperty(DefineProperty.RADIUS)]
        public double radius { get; set; }

        [JsonProperty(DefineProperty.SWEEP)]
        public double sweep { get; set; }

        [JsonProperty(DefineProperty.ORIGIN_U)]
        public double origin_u { get; set; }

        [JsonProperty(DefineProperty.ORIGIN_V)]
        public double origin_v { get; set; }

        [JsonProperty(DefineProperty.U)]
        public double u { get; set; }

        [JsonProperty(DefineProperty.V)]
        public double v { get; set; }

        double getRadius()
        {
            return radius;
        }

        double getOrigin_U()
        {
            return origin_u;
        }

        double getOrigin_V()
        {
            return origin_v;
        }
    }
}
