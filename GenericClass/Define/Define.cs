﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass.Define
{
    public static class GenericData
    {
        public const string GENERAL_DATA = "GENERAL_DATA";
        public const string END_OF_GENERAL_DATA = "END_OF_GENERAL_DATA";

        public const string PART_DATA = "PART_DATA";
        public const string END_OF_PART_DATA = "END_OF_PART_DATA";

        public const string LABELTEXT_DATA = "LABELTEXT_DATA";
        public const string END_OF_LABELTEXT_DATA = "END_OF_LABELTEXT_DATA";

        public const string PART_INFORMATION = "PART_INFORMATION";
        public const string END_OF_PART_INFORMATION = "END_OF_PART_INFORMATION";

        public const string IDLE_DATA = "IDLE_DATA";
        public const string END_OF_IDLE_DATA = "END_OF_IDLE_DATA";

        public const string MARKING_DATA = "MARKING_DATA";
        public const string END_OF_MARKING_DATA = "END_OF_MARKING_DATA";

        public const string BURNING_DATA = "BURNING_DATA";
        public const string END_OF_BURNING_DATA = "END_OF_BURNING_DATA";

        public const string BEVEL_DATA = "BEVEL_DATA";
        public const string END_OF_BEVEL_DATA = "END_OF_BEVEL_DATA";

        public const string START_OF_CONTOUR = "START_OF_CONTOUR";
        public const string END_OF_CONTOUR = "END_OF_CONTOUR";

    }

    public static class DefineValue
    {
        public const double FootSensorDist = 60.0;
        public const string SpeedTableFilePath = @"D:\NAVANTIA\20160215_bv_table\SF842DBEV_ESSI(R0)_TAB.DAT";
    }

    
}
