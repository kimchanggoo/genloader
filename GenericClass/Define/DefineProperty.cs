﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass.Define
{
    public static class DefineProperty
    {
        //General
        public const string NEST_NAME = "NEST_NAME";
        public const string RAW_LENGTH = "RAW_LENGTH";
        public const string RAW_WIDTH = "RAW_WIDTH";
        public const string RAW_THICKNESS = "RAW_THICKNESS";
        public const string QUALITY = "QUALITY";

        //Label Text Data
        public const string TEXT_TYPE = "TEXT_TYPE";
        public const string TEXT_POSITION_U = "TEXT_POSITION_U";
        public const string TEXT_POSITION_V = "TEXT_POSITION_V";
        public const string TEXT_ANGLE = "TEXT_ANGLE";
        public const string TEXT_HEIGHT = "TEXT_HEIGHT";
        public const string TEXT = "TEXT";
        public const string TEXT_ELEMENTS = "TEXT_ELEMENTS";

        //Bevel Data
        public const string BEVEL = "BEVEL";
        public const string BEVEL_CODE = "BEVEL_CODE";
        public const string BEVEL_TYPE = "BEVEL_TYPE";
        public const string PLATE_THICKNESS = "PLATE_THICKNESS";
        public const string ANGLE_TS = "ANGLE_TS";
        public const string ANGLE2_TS = "ANGLE2_TS";
        public const string ANGLE_OS = "ANGLE_OS";
        public const string ANGLE2_OS = "ANGLE2_OS";
        public const string DEPTH_TS = "DEPTH_TS";
        public const string DEPTH_OS = "DEPTH_OS";
        public const string CHAMFER_WIDTH_TS = "CHAMFER_WIDTH_TS";
        public const string CHAMFER_WIDTH_OS = "CHAMFER_WIDTH_OS";
        public const string ANGLE2_WTS = "ANGLE2_WTS";
        public const string ANGLE2_WOS = "ANGLE2_WOS";
        public const string CHAMFER_HEIGHT_TS = "CHAMFER_HEIGHT_TS";
        public const string CHAMFER_HEIGHT_OS = "CHAMFER_HEIGHT_OS";

        //Part Information
        public const string PART_NAME = "PART_NAME";
        public const string TYPE_OF_WORK = "TYPE_OF_WORK";

        //Seg Info
        public const string AMP_U = "AMP_U";
        public const string AMP_V = "AMP_V";
        public const string AMP = "AMP";
        public const string RADIUS = "RADIUS";
        public const string SWEEP = "SWEEP";
        public const string ORIGIN_U = "ORIGIN_U";
        public const string ORIGIN_V = "ORIGIN_V";
        public const string U = "U";
        public const string V = "V";

        //Burning Data
        public const string SHAPE = "SHAPE";
        public const string START_END_IN_GAP = "START_END_IN_GAP";
        public const string BEVEL_DEFINED = "BEVEL_DEFINED";
        public const string DIRECTION = "DIRECTION";
        public const string NUMBER_OF_HEADS = "NUMBER_OF_HEADS";
        public const string GEOMETRY_VALID_FOR = "GEOMETRY_VALID_FOR";
        public const string DISTANCE_Y1_Y2 = "DISTANCE_Y1_Y2";

        //Con Info
        public const string NO_OF_SEG = "NO_OF_SEG";
        public const string START_U = "START_U";
        public const string START_V = "START_V";



        //Value
        public const string START_HOOK = "START_HOOK";
        public const string END_HOOK = "END_HOOK";
        public const string OUTER_CONTOUR = "OUTER_CONTOUR";
        public const string HOLE = "HOLE";
        public const string CORNER_LOOP = "CORNER_LOOP";





    }
}
