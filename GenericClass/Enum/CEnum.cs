﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass.Enum
{
    public enum CONTOUR_TYPE
    {
        IDLE_DATA = 0,
        MARKING_DATA = 1,
        BURNING_DATA = 2,
    }

    public enum LineType
    {
        Line = 0,
        Arc = 1,
    }

    public enum NCType
    {
        IDLE = 0,
        OUT = 1,
        IN = 2,
        MARKING = 3,
    }

    public enum NCSubType
    {
        NONE = 0,
        LEAD_IN = 1,
        LEAD_OUT = 2,
        CONER_LOOP = 3,
        CUT = 4,
        FOOTSENSOR = 5,
    }

    public enum NCSpeed
    {
        NORMAL = 0,
        ADJUSTED = 1,
    }

    public enum Machine
    {
        None = 0,
        TBC = 1,
        TBP = 2,
        TEP = 3,
    }

    public enum CuttingOption
    {
        None = 0,
        C = 1,
        CC = 2,
        CM = 3,
    }

}
