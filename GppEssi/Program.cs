﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GenericClass;
using GenericClass.Define;
using GenericClass.Enum;

namespace GppEssi
{
    class Program
    {
        static void Main(string[] args)
        {
            string speedFilePath = "";
            bool bWriteLetterMark = false;
            bool bWriteLineMarking = false;
            bool bWriteSpeed = false;
            bool bWriteBevel = false;
            CuttingOption cuttingOption = CuttingOption.None;
            string genericFilePath = "";
            string essiFilePath = "";
            Machine machinePackage = Machine.None;

            if (args.Length != 16)
            {
                Console.WriteLine("GppEssi.exe -D Speed_File -L YES -M YES -S YES -B YES -G Generic_File -T TBP_TBC_TEP -C C_CC_CM");
                Console.WriteLine("-D <Speed file> : 스피드 Data 파일");
                Console.WriteLine("-L <Yes/No> : Letter Marking 적용 여부");
                Console.WriteLine("-M <Yes/No> : Line Marking 적용 여부");
                Console.WriteLine("-S <Yes/No> : Speed 적용 여부");
                Console.WriteLine("-B <Yes/No> : 개선 적용 여부");
                Console.WriteLine("-G <Generic File> : Generic File");
                Console.WriteLine("-T <TBC/TBP/TEP> : Work Package");
                Console.WriteLine("-C <C/CC/CM> : Cutting Option");

                Console.Write("Press any key to continue..");
                Console.ReadKey();

                return;
            }

            for (int i = 0; i < args.Count(); i++ )
            {
                switch(args[i].ToUpper())
                {
                    case "-D":
                        speedFilePath = args[i + 1];
                        break;
                    case "-L":
                        if (args[i + 1].ToUpper().Equals("YES")) bWriteLetterMark = true;
                        break;
                    case "-M":
                        if (args[i + 1].ToUpper().Equals("YES")) bWriteLineMarking = true;
                        break;
                    case "-S":
                        if (args[i + 1].ToUpper().Equals("YES")) bWriteSpeed = true;
                        break;
                    case "-B":
                        if (args[i + 1].ToUpper().Equals("YES")) bWriteBevel = true;
                        break;
                    case "-G":
                        genericFilePath = args[i + 1];
                        break;
                    case "-T":
                        if (args[i + 1].ToUpper().Equals("TBC")) machinePackage = Machine.TBC;
                        else if (args[i + 1].ToUpper().Equals("TBP")) machinePackage = Machine.TBP;
                        else if (args[i + 1].ToUpper().Equals("TEP")) machinePackage = Machine.TEP;
                        break;
                    case "-C":
                        if (args[i + 1].ToUpper().Equals("C")) cuttingOption = CuttingOption.C;
                        else if (args[i + 1].ToUpper().Equals("CC")) cuttingOption = CuttingOption.CC;
                        else if (args[i + 1].ToUpper().Equals("CM")) cuttingOption = CuttingOption.CM;
                        break;
                    default:
                        break;

                }
            }

            if (machinePackage == Machine.None)
            {
                Console.WriteLine("Machine type is Invalid");
                return;
            }

            if (cuttingOption == CuttingOption.None)
            {
                Console.WriteLine("cuttingOption is Invalid");
                return;
            }

            FileInfo fileInfo = new FileInfo(genericFilePath);
            
            essiFilePath = genericFilePath.Substring(0, genericFilePath.IndexOf(fileInfo.Extension)) + ".mpg";

            CGenInfo cGenInfo = new CGenInfo();
            cGenInfo.bWriteText = bWriteLetterMark;
            cGenInfo.bWriteMarking = bWriteLineMarking;
            cGenInfo.bWriteSpeed = bWriteSpeed;
            cGenInfo.bWriteBevel = bWriteBevel;
            cGenInfo.SpeedFilePath = speedFilePath;
            cGenInfo.GenericFilePath = genericFilePath;
            cGenInfo.EssiFilePath = essiFilePath;
            cGenInfo.MachinePackage = machinePackage;
            cGenInfo.CuttingOptionPackage = cuttingOption;

            bool bOk = ConvertGeneric(cGenInfo);

            if (bOk)
            {
                Console.WriteLine("[OK] {0}", essiFilePath);
            }
            else
            {
                if (cGenInfo.bError)
                {
                    foreach (var item in cGenInfo.err_info)
                    {
                        Console.WriteLine("[0]", item);
                    }
                }
            }

        }

        static bool ConvertGeneric(CGenInfo cGenInfo)
        {
            bool bRtn = true;

            if (!cGenInfo.LoadSpeedTable())
            {
                return false;
            }

            if (!cGenInfo.LoadGenericFile())
            {
                return false;
            }

            cGenInfo.SetGeoList();

            if (!cGenInfo.ConvertEssiCode())
            {
                return false;
            }

            return bRtn;
        }

    }
}
